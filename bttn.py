from settings import *
import pygame






class Button:

    def __init__(self, x, y, width, height, text):

        self.x = x
        self.y = y
        self.width = width
        self.height = height

        self.font = pygame.font.SysFont("Arial", 30)
        self.text = text

        self.status = 0

    def draw(self, window):
        if self.status == 0:
            color = COLOR["white"]
        elif self.status == 1:
            color = COLOR["green"]
        elif self.status == 2:
            color = COLOR["yellow"]
        elif self.status == -1:
            color = COLOR["grey"]
        pygame.draw.rect(window, color, (self.x, self.y, self.width, self.height), 0)
        pygame.draw.rect(window, (0,0,0), (self.x, self.y, self.width, self.height), 2)
        text_surface = self.font.render(self.text, True, (0,0,0))
        window.blit(text_surface, (self.x + (self.width/2 - text_surface.get_width()/2), self.y + (self.height/2 - text_surface.get_height() / 2)))

    def is_hover(self, pos):
        if pos[0] > self.x and pos[0] < self.x + self.width:
            if pos[1] > self.y and pos[1] < self.y + self.height:
                return True
        return False

    def click(self, buttons=None):
        global CURRENT_INDEX
        global word_list
        if CURRENT_INDEX == 4:
            if word_list[CURRENT_ROW][CURRENT_INDEX]["letter"] == "":
                word_list[CURRENT_ROW][CURRENT_INDEX]["letter"] = self.text
        else:
            word_list[CURRENT_ROW][CURRENT_INDEX]["letter"] = self.text
            CURRENT_INDEX += 1
        print("-" * 60)
        print(f"debug button index: {CURRENT_INDEX}")
            

    def set_status(self, num):
        self.status = num

    def get_status(self):
        return self.status

    def get_text(self):
        return self.text

class DeleteButton(Button):

    def click(self, buttons=None):
        global CURRENT_INDEX
        global word_list
        if CURRENT_INDEX == 0:
            word_list[CURRENT_ROW][CURRENT_INDEX]["letter"] = ""
        else:
            if (CURRENT_INDEX - 2) > -1 and word_list[CURRENT_ROW][CURRENT_INDEX]["letter"] == "":
                CURRENT_INDEX -= 1
                word_list[CURRENT_ROW][CURRENT_INDEX]["letter"] = ""
                CURRENT_INDEX -= 1
            else:
                word_list[CURRENT_ROW][CURRENT_INDEX]["letter"] = ""
                CURRENT_INDEX -= 1
        print("-" * 60)
        print(f"debug del index: {CURRENT_INDEX}")

class EnterButton(Button):

    def click(self, buttons):
        global CURRENT_ROW, CURRENT_INDEX, WORD
        WORD = WORD.upper()
        word = ""
        for item in word_list[CURRENT_ROW]:
            word += item["letter"]

        if CURRENT_INDEX == 4  and word_list[CURRENT_ROW][CURRENT_INDEX]["letter"] != "":
            
            print(f"check: {word}")


            for index, content in enumerate(word):
                print(f"index : {index}")
                print(f"content : {content}")
                print("-" * 80)
                if content == WORD[index]:
                    for button in buttons:
                        if button.get_text() == content:
                            button.set_status(1)
                            word_list[CURRENT_ROW][index]["status"] = 1
                            print(f"button status {button.get_status()}")
                elif content in WORD:
                    for button in buttons:
                        if button.get_text() == content:
                            if button.get_status() != 1:
                                button.set_status(2)
                            word_list[CURRENT_ROW][index]["status"] = 2
                            print(f"button status {button.get_status()}")
                elif content not in WORD:
                    for button in buttons:
                        if button.get_text() == content:
                            button.set_status(-1)
                            word_list[CURRENT_ROW][index]["status"] = -1
                            print(f"button status {button.get_status()}")
                
            
            CURRENT_ROW += 1
            CURRENT_INDEX = 0

            

        if word == WORD:
            WORD = create_new_word()
            for row in range(6):
                for index in range(5):
                    word_list[row][index]["letter"] = ""
                    word_list[row][index]["status"] = 0
            
            for button in buttons:
                button.set_status(0)

            CURRENT_ROW = 0
            CURRENT_INDEX = 0

        print("-" * 60)
        print(f"debug enter index: {CURRENT_INDEX}")
        print(f"debug enter row: {CURRENT_ROW}")





        




class Keyboard:

    def __init__(self, x, y, width, height, margin):
        self.x = x
        self.y = y
        self.width = width
        self.height = height
        self.margin = margin

        self.buttons = []
        self.create_keyboard()

    def create_keyboard(self):
        x_offset = 0
        y_offset = 0
        for letter in alphabet:
            if letter == "x":
                self.x = 105
                button = Button(self.x + x_offset, self.y + y_offset, self.width, self.height, letter.upper())
                self.buttons.append(button)
                x_offset += self.width + self.margin
                if x_offset >= (self.width + self.margin) * 10:
                    x_offset = 0
            else:
                button = Button(self.x + x_offset, self.y + y_offset, self.width, self.height, letter.upper())
                self.buttons.append(button)
                x_offset += self.width + self.margin
                if x_offset >= (self.width + self.margin) * 10:
                    x_offset = 0
                    y_offset += self.height + self.margin
        
        delete = DeleteButton(405, 450, 90, 40, "DEL")
        self.buttons.append(delete)
        enter = EnterButton(5, 450, 90, 40, "ENTER")
        self.buttons.append(enter)

    def draw(self, window):
        for button in self.buttons:
            button.draw(window)

    def handle_click(self, pos):
        for button in self.buttons:
            if button.is_hover(pos):
                button.click(self.buttons)
                    






class Field:

    def __init__(self, x, y, width, height):
        self.x = x
        self.y = y
        self.height = height
        self.width = width

        self.font = pygame.font.SysFont("Arial", 30)

    def draw(self, window, text, color):
                
        pygame.draw.rect(window, color, (self.x, self.y, self.width, self.height), 0)
        pygame.draw.rect(window, (0,0,0), (self.x, self.y, self.width, self.height), 2)
        text_surface = self.font.render(text, True, (0,0,0))
        window.blit(text_surface, (self.x + (self.width/2 - text_surface.get_width()/2), self.y + (self.height/2 - text_surface.get_height() / 2)))


class EntryFields:

    def __init__(self, x, y, width, height, margin):
        
        self.x = x
        self.y = y
        self.width = width
        self.height = height
        self.margin = margin

        self.fields = []
        self.create_fields()

    def create_fields(self):
        
        x_offset = 0
        y_offset = 0

        for i in range(6):
            for j in range(5):
                field = Field(self.x + x_offset, self.y + y_offset, self.width, self.height)
                self.fields.append(field)
                x_offset += self.width + self.margin
                if x_offset >= (self.width + self.margin) * 5:
                    x_offset = 0
                    y_offset += self.height + self.margin

    def update(self):
        global CURRENT_ROW, CURRENT_INDEX, word_list

        if CURRENT_ROW == 6:
            for row in range(6):
                for index in range(5):
                    word_list[row][index]["letter"] = ""
                    word_list[row][index]["status"] = 0

            CURRENT_ROW = 0
            CURRENT_INDEX = 0
                
        

    def draw(self, window):

        index = 0
        for row in word_list:
            for field in row:
                text = field["letter"]
                status = field["status"]
                if status == 0:
                    color = COLOR["white"]
                elif status == 1:
                    color = COLOR["green"]
                elif status == 2:
                    color = COLOR["yellow"]
                elif status == -1:
                    color = COLOR["grey"]
                self.fields[index].draw(window, text, color)
                index += 1
        
        
            