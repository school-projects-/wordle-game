import pygame 
from settings import *
from bttn import *




pygame.init()


screen = pygame.display.set_mode((WIDTH, HEIGHT))
pygame.display.set_caption("keyboard")

keyboard = Keyboard(5, 350, 40, 40, 10)
entry = EntryFields(100, 15, 50, 50, 5)




run = True


while run == True:
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            exit()
        if event.type == pygame.MOUSEBUTTONDOWN:
            pos = pygame.mouse.get_pos()
            keyboard.handle_click(pos)

    screen.fill((100, 100, 100))
    keyboard.draw(screen)
    entry.draw(screen)
    entry.update()
    
    pygame.display.update()