import random



with open("words.txt", "r") as f:
    words = [file.strip() for file in f]



def create_new_word():
    global WORD
    WORD = random.choice(words)
    print(WORD)
    return WORD


WIDTH = 500
HEIGHT = 500


COLOR = {
    "white" : (255, 255, 255),
    "black" : (0, 0, 0),
    "green" : (106, 170, 100),
    "yellow" : (201, 180, 88),
    "grey" : (120, 124, 126),
    "outline" : (211, 214, 218),
    "filled_outline" : (135, 138, 140)
}

word_list = [
    [
        {
            "letter" : "",
            "status" : 0
        },
        {
            "letter" : "",
            "status" : 0
        },
        {
            "letter" : "",
            "status" : 0
        },
        {
            "letter" : "",
            "status" : 0
        },
        {
            "letter" : "",
            "status" : 0
        }
    ],
    [
        {
            "letter" : "",
            "status" : 0
        },
        {
            "letter" : "",
            "status" : 0
        },
        {
            "letter" : "",
            "status" : 0
        },
        {
            "letter" : "",
            "status" : 0
        },
        {
            "letter" : "",
            "status" : 0
        }
    ],
    [
        {
            "letter" : "",
            "status" : 0
        },
        {
            "letter" : "",
            "status" : 0
        },
        {
            "letter" : "",
            "status" : 0
        },
        {
            "letter" : "",
            "status" : 0
        },
        {
            "letter" : "",
            "status" : 0
        }
    ],
    [
        {
            "letter" : "",
            "status" : 0
        },
        {
            "letter" : "",
            "status" : 0
        },
        {
            "letter" : "",
            "status" : 0
        },
        {
            "letter" : "",
            "status" : 0
        },
        {
            "letter" : "",
            "status" : 0
        }
    ],
    [
        {
            "letter" : "",
            "status" : 0
        },
        {
            "letter" : "",
            "status" : 0
        },
        {
            "letter" : "",
            "status" : 0
        },
        {
            "letter" : "",
            "status" : 0
        },
        {
            "letter" : "",
            "status" : 0
        }
    ],
    [
        {
            "letter" : "",
            "status" : 0
        },
        {
            "letter" : "",
            "status" : 0
        },
        {
            "letter" : "",
            "status" : 0
        },
        {
            "letter" : "",
            "status" : 0
        },
        {
            "letter" : "",
            "status" : 0
        }
    ]
]


CURRENT_ROW = 0
CURRENT_INDEX = 0

CURRENT_LETTER = ""

alphabet = "qwertyuiopasdfghjklzxcvbnm"

WORD = random.choice(words)

